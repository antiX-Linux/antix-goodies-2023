��          �   %   �      P  7   Q  
   �  4   �     �  !   �  ;   �  P   5  B   �  Y   �  U   #  I   y  d   �  8   (  \   a  %   �     �     �  �   �  Z   �  )   �  ,     1   2  )   d  `   �  \   �  �  L  l   �	     T
  b   f
     �
  -   �
  j     �   n  �   )  �   �  �   {  �   =  �   �  p   �  �     c   �  .        J  �   X  �   �  X   �  Y   �  }   I  H   �  �     �   �                                	                                                                             
                  GUI tool for safely unplugging removable USB devices.   Options:   Questions, Suggestions and Bugreporting please to:   Unplugdrive   Usage: unplugdrive.sh [options] <b>Aborting</b> on user request\n<b>without unmounting.</b> <b>About to unmount:</b>\n$summarylist<b>Please confirm you wish to proceed.</b> <b>Check each mountpoint listed before unpluging the drive(s).</b> <b>Mounted USB partitions:</b>\n$removablelist<b>Choose the drive(s) to be unplugged:</b> <b>Mountpoint removal failed.</b>\n<u>One or more mountpoin(s) remain present at:</u> <b>Unmounted:</b>\n$summarylist\n<b>It is safe to unplug the drive(s)</b> A removable drive with a mounted\npartition was not found.\n<b>It is safe to unplug the drive(s)</b> Data is being written\nto devices. <b>Please wait...</b> Invalid command line argument. Please call\nunplugdrive -h or --help for usage instructions. Taskbar icon $icon_taskbar not found. Unplug USB device Version  \033[1;31mWARNING: DEVICE ${deviceslist[u]} WAS NOT PROPERLY UNMOUNTED!\033[0m\n\033[4;37mPlease check before unplugging.\033[0m \nˮyadˮ is not installed.\n   --> Please install ˮyadˮ before executing this script.\n \t-c or --centred\t\topen dialogs centred \t-d or --decorated\tuses window decorations \t-g or --debug\t\tenable debug output (terminal) \t-h or --help\t\tdisplays this help text \t-p or --pretend\t\tdry run, don't actually un-\n\t            \t\tmount drives (for debugging) \t-s or --safe\t\truns script in safe mode,\n\t            \t\tdisplaying some extra dialogs Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-10-27 11:12+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021
Language-Team: Greek (http://www.transifex.com/anticapitalista/antix-development/language/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Εργαλείο GUI για ασφαλή αποσύνδεση αφαιρούμενων συσκευών USB. Επιλογές: Ερωτήσεις, προτάσεις και αναφορά σφαλμάτων, παρακαλώ: Unplugdrive Χρήση: unplugdrive.sh [επιλογές] <b>Ματαίωση</b> μετά από αίτημα χρήστη
<b> χωρίς αποσύνδεση.</b> <b>Είναι σε εξέλιξη η αποπροσάρτηση:</b>
$summarylist </b>Παρακαλώ επιβεβαιώστε ότι επιθυμείτε να συνεχίσετε.</b> <b>Ελέγξτε κάθε σημείο προσάρτησης που αναφέρεται πριν αποσυνδέσετε τις μονάδες δίσκου</b> <b>Τοποθετημένα διαμερίσματα USB:</b>
$ removablelist
<b>Επιλέξτε τις μονάδες δίσκου που θα αποσυνδεθούν</b> <b>Η απομάκρυνση του MountPoint απέτυχε.</b>\n <u>Κανένα ή περισσότερα ή περισσότερα mountpoint(s) παραμένουν παρόντα σε:  <b>Αποσυνδεδεμένος:</b>\n$summarylist\n<b>Είναι ασφαλές να αποσυνδέσετε τη μονάδα δίσκου</b>  Μια αφαιρούμενη μονάδα δίσκου με ένα τοποθετημένο διαμέρισμα δεν βρέθηκε.
<b>Είναι ασφαλές να την αποσυνδέσετε.</b> Τα δεδομένα γράφονται
σε συσκευές. <b>Παρακαλώ περιμένετε ...</b> Μη έγκυρο όρισμα γραμμής εντολών. Παρακαλούμε καλέστε \nunplugdrive -h ή --help για οδηγίες χρήσης. Το εικονίδιο της γραμμής εργασιών $icon_taskbar δεν βρέθηκε. Αποσυνδέστε τη συσκευή USB Έκδοση  ΠΡΟΕΙΔΟΠΟΙΗΣΗ: Η συσκευή δεν ήταν σωστά μη αποσταθεράτε! Ελέγξτε πριν αποσυνδέσετε.  \nˮyadˮ δεν είναι εγκατεστημένο.\n  --> Εγκαταστήστε το ˮYadˮ πριν εκτελέσετε αυτό το σενάριο.\n \t-c η --centred\t\tΆνοιγμα διαλόγων που επικεντρώνοντα \t-d η --decorated\tΧρησιμοποιεί διακοσμήσεις παραθύρων \t-g η --debug\t\tΕνεργοποιήστε την έξοδο εντοπισμού σφαλμάτων (τερματικό)     \t-h η --help\t\tΕμφάνιση αυτής της Βοήθειας \t-p η --pretend\t\tστεγνή πορεία, μην αποσυναρμολογήσετε τις μονάδες δίσκου (για σφαλμάτωση) \t-s η --safe\t\tΕκτελεί σενάριο σε ασφαλή λειτουργία,\n\t \t\Εμφάνιση ορισμένων επιπλέον διαλόγων 